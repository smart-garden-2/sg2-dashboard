module.exports = {
  root: true,
  env: {
    node: true
  },
  plugins: ["only-warn"],
  extends: ["plugin:vue/vue3-recommended"],
  parserOptions: {
    ecmaVersion: 2020
  }
};
