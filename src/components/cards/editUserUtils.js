import bcrypt from "bcryptjs";

export function makeEmailEditBody(NewEmail) {
    return {
        email: NewEmail
    }
}

export function makeEditNameReqBody(newName) {
    return {
        name: newName
    }
}

export function makeEditSurnameReqBody(newSurname) {
    return {
        surname: newSurname
    }
}

export function makeEditPasswordBody(newPassword) {
    return {
        password: bcrypt.hashSync(newPassword, 10)
    }
}
