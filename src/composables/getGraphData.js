import { serverURL } from "@/env";
import { get } from "@/composables/useAxios";
import { handleAuthentication } from "@/utils/authUtils";

// { value: 29, timestamp: 1613865600000000000 },
// { value: 32, timestamp: 1613829600000000000 },
// { value: 26, timestamp: 1613815200000000000 },
// { value: 25, timestamp: 1613782800000000000 },
// { value: 27, timestamp: 1613768400000000000 }

const removeAllWhitespaces = (string) => {
    return string.replace(/\s/g, '')
}

const lowercaseFirstLetter = (string) => {
    return string.charAt(0).toLowerCase() + string.slice(1)
}

export const useTelemetryGatherer = async (data, measurement, period) => {
    period = period.toLowerCase()
    measurement = removeAllWhitespaces(measurement)
    measurement = lowercaseFirstLetter(measurement)

    await get(`${serverURL}/telemetry/${period}/${measurement}`)
        .then(res => {
            if(res.status !== 204) {
                data.value = res.data
            }
        }).catch(err => {
            handleAuthentication(err)
        })
}
