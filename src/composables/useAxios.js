import axios from "axios";
import { serverURL } from "@/env";
import { useCookies } from "vue3-cookies";

export function del(url, reqBody = undefined) {
    const { cookies } = useCookies()
    const token = cookies.get("token")
    const req = {
        baseURL: serverURL,
        url: url,
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
        },
        method: "DELETE",
        responseType: "json",
    }
    if (reqBody) req.data = reqBody
    return axios(req)
}

export function post(url, reqBody = undefined) {
    const { cookies } = useCookies()
    const token = cookies.get("token")
    const req = {
        baseURL: serverURL,
        url: url,
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
        },
        method: "POST",
        responseType: "json",
    }
    if (reqBody) req.data = reqBody
    return axios(req)
}

export function get(url) {
    const { cookies } = useCookies()
    const token = cookies.get("token")
    return axios({
        baseURL: serverURL,
        url: url,
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
        },
        method: "GET",
        responseType: "json",
    })
}

export function put(url, reqBody) {
    const { cookies } = useCookies()
    const token = cookies.get("token")
    return axios({
        baseURL: serverURL,
        url: url,
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
        },
        method: "PUT",
        data: reqBody,
        responseType: "json",
    })
}
