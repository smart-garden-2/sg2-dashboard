import { get } from "@/composables/useAxios";
import { serverURL } from "@/env";

export function usePlanGatherer() {
    const email = localStorage.getItem("email")
    return get(`${serverURL}/irrigationPlans/${email}`)
}
