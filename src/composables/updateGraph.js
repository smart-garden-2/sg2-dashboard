import * as d3 from "d3";

export function useUpdateGraphDimensions(width, height, svg, data) {
    data = data.filter(v => {
        return v.value != null
    })
    data.map(v => {
        v.timestamp = d3.isoParse(v.timestamp)
    })

    const xScale = d3.scaleUtc()
        .domain(d3.extent(data, function(d) {
            return d.timestamp
        })).nice()
        .range([0, width])

    const yScale = d3.scaleLinear()
        .domain([0, d3.max(data, d => d.value)]).nice()
        .range([height, 0])

    const lineGen = d3.line()
        .curve(d3.curveBasis)
        .x((d) => xScale(d.timestamp))
        .y((d) => yScale(d.value))

    // render a path element with D3's General Update Pattern
    svg.selectAll(".line")
        .data([data])
        .join("path")
        .attr("class", "line")
        .attr("clip-path", "url(#clip)")
        .attr("stroke", "green")
        .attr("d", lineGen)

    const xAxis = d3.axisBottom(xScale)
    svg.select(".x-axis")
        .style("transform", `translateY(${height}px)`)
        .call(xAxis)
    const yAxis = d3.axisLeft(yScale)
    svg.select(".y-axis")
        .call(yAxis)
}
