export const getDate = (date) => {
    return `${new Date(date).toLocaleDateString()}`
}

export const getTime = (time) => {
    return `${new Date(time).toLocaleTimeString()}`
}

export const getDays = (days) => {
    return days.toString().replace(/,/g, ", ")
}

export const getFormattedCurrentDate = () => {
    let today = new Date()
    const dd = String(today.getDate()).padStart(2, '0')
    const mm = String(today.getMonth() + 1).padStart(2, '0')
    const yyyy = today.getFullYear()
    return mm + '/' + dd + '/' + yyyy
}
