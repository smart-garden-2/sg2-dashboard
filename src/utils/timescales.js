export const Timescales = {
    Day : "Day",
    Week: "Week",
    Month: "Month",
    Year: "Year",
    RealTime: "RealTime"
}

export const parseTimescale = (period) => {
    return Timescales[period]
}
