import { io } from "socket.io-client";
import { serverApiDomain, serverApiPort } from "@/env";

class SocketIOService {
    setupSocketConnection() {
        this.socket = io(`ws://${serverApiDomain}:${serverApiPort}`, {
            reconnectionDelay: 30000,
            reconnectionDelayMax: 40000,
        })
        return this.socket
    }
}

export default new SocketIOService()
