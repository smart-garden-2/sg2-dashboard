export const Measurements = {
    SoilHumidity: "Soil Humidity",
    AirHumidity: "Air Humidity",
    Temperature: "Temperature",
    Pressure: "Pressure",
    TankLevel: "Tank Level"
}

export const parseMeasurementIndex = (index) => {
    switch (index) {
        case "Soil Humidity":
            return 3
        case "Air Humidity":
            return 0
        case "Temperature":
            return 2
        case "Pressure":
            return 1
        case "Tank Level":
            return 0
    }
}

export const parseMeasurement = (measurement) => {
    // remove all whitespaces
    return Measurements[measurement.replace(/\s/g, '')]
}
