import router from "@/router";
import { useCookies } from "vue3-cookies";

export function isAuthenticated(cookies) {
    return cookies.get("token") !== null
}

export async function handleAuthentication(err) {
    const { cookies } = useCookies()
    if (err.response.status === 401) {
        localStorage.clear()
        cookies.remove("token")
        await router.push("/login")
    }
}
