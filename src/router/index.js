import { createRouter, createWebHashHistory } from "vue-router";
import DashboardGraphs from "@/views/DashboardGraphs.vue";
import LoginPage from "@/views/LoginPage";
import RegistrationPage from "@/views/RegistrationPage";
import UserSettings from "@/views/UserSettings";
import EspSettings from "@/views/EspSettings";
import BaseLayout from "@/components/layouts/BaseLayout";
import ManagePlans from "@/views/ManagePlans";
import PageNotFound from "@/views/PageNotFound";
import CreatePlan from "@/views/CreatePlan";
import { useCookies } from "vue3-cookies";
import { isAuthenticated } from "@/utils/authUtils";

const routes = [
    {
        path: "/",
        redirect: "/dashboard",
    },
    {
        path: "/dashboard",
        redirect: "/dashboard/graphs",
        component: BaseLayout,
        children: [
            {
                path: "/dashboard/graphs",
                name: "graphs",
                component: DashboardGraphs,
            },
            {
                path: "/dashboard/managePlans",
                component: ManagePlans,
            },
            {
                path: "/dashboard/createPlan",
                component: CreatePlan,
            },
            {
                path: "/dashboard/userSettings",
                name: "userSettings",
                component: UserSettings,
            },
            {
                path: "/dashboard/espSettings",
                component: EspSettings,
            },
        ],
    },
    {
        path: "/register",
        name: "register",
        component: RegistrationPage,
    },
    {
        path: "/login",
        name: "login",
        component: LoginPage,
    },
    {
        path: "/404",
        component: PageNotFound,
    },
    {
        path: "/:pathMatch(.*)*",
        redirect: "/404",
    },
];

const router = createRouter({
    history: createWebHashHistory(),
    routes,
});

router.beforeResolve(async (to) => {
    const { cookies } = useCookies();
    if (
        !isAuthenticated(cookies) &&
        to.name !== "register" &&
        to.name !== "login"
    )
        return "/login";
    // if(from.path === "/register") return "/login";
});

export default router;
