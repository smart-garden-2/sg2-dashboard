import SocketIOService from "@/utils/socketIOService";
import { get } from "@/composables/useAxios";

const socket = SocketIOService.setupSocketConnection()

export const useStateUpdater = (currentState) => {
    socket.on("system/state", (msg) => {
        currentState.value = getSystemState(msg)
    })
}

export const useRealTimeDataGatherer = (telemetryData, tankLevelData) => {
    socket.on("system/telemetry", (msg) => {
        Object.assign(telemetryData.value, JSON.parse(msg))
    });
    socket.on("system/tankLevel", (msg) => {
        Object.assign(tankLevelData.value, JSON.parse(msg))
    })
};

export const getNotify = (notificationArray) => {
    socket.on("notification", (msg) => {
        notificationArray.push(JSON.parse(msg))
    });
    const email = localStorage.getItem("email")
    get(`/notifications/${email}`).then((result) => {
        if (result.status === 200) {
            result.data.description.map((el) => {
                notificationArray.push(el.notification)
            })
        }
    })
}

function getSystemState(msg) {
    const state = JSON.parse(msg)
    switch (state.value) {
        case 0:
            return "IDLE"
        case 1:
            return "WATERING MANUAL"
        case 2:
            return "WATERING AUTOMATIC"
        case 3:
            return "MONITOR"
        case 4:
            return "ALARM"
    }
}
