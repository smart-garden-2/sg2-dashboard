import { createApp } from "vue";
import App from "./App";
import router from "./router";
import VueClickAway from "vue3-click-away";
import VueCookies from "vue3-cookies";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "./styles/app.css";

const app = createApp(App);
app.use(VueCookies);
app.use(VueClickAway);

/*
 * Setup routes.
 */
app.use(router);

/*
 * Start the app.
 */
app.mount("#app");
