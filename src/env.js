const serverApiDomain = "localhost";
const serverApiPort = 3001;
const serverURL = `http://${serverApiDomain}:${serverApiPort}`;
const cookieDuration = "1h";

module.exports = {
    serverURL,
    serverApiPort,
    serverApiDomain,
    cookieDuration,
};
